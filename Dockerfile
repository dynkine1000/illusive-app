FROM python:3.9-slim
ADD requirements.txt /
RUN pip install -r requirements.txt
ADD main.py /
USER 10001
CMD [ "python", "./main.py" ]
