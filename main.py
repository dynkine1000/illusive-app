import os
import yaml
import tempfile
from pathlib import Path
from kubernetes import client, config
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
from sanic import Sanic, json
from sanic.response import text
import requests, json
from sanic import response

app = Sanic("CodeToAPI")
HOST = "0.0.0.0"
PORT = 8000

def gen_client():
    config.load_incluster_config()
    return client.CoreV1Api()


print("Listing pods with their IPs:")

@app.route('/info')
async def getpods(request):
        v1 = gen_client()
        ret = v1.list_pod_for_all_namespaces(watch=False)
        result = []
        for i in ret.items:
            pod = i.metadata.name
            result.append(pod)
        return response.json(result)

if __name__ == "__main__":
    app.run(host=HOST, port=PORT)
