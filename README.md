
## This pipeline creats my-app docker iamge, pushes it to ECR and deploys my-app application to EKS cluster.

## Once the image is built and chart is deployed, we can get the load nalancer address
```bash
aws elb describe-load-balancers | grep DNSName: | sed  's/DNSName: /http:\/\//g'
```
## Then we cal get the nemes of all pods in the cluster by accessig via GET our app et endpoint - /info

There are three ways to access the aplication:

1. Via Curl commnand to load-balancer-DNS-name-from-previous-step

```bash
curl http://a474156ca5d134e9db5a756d9abcdd8b-1863427484.us-east-1.elb.amazonaws.com:8000/info
```
2. Using Postman app

3. Via browser

### ! Please note that you shoud set your AWS access key and secret ley as varibles in GitlabCI !